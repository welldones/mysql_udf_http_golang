
FROM mariadb:10.7.1


RUN apt-get update  && apt-get  install -y musl-dev  && \        
        ln -s /usr/lib/x86_64-linux-musl/libc.so /lib/libc.musl-x86_64.so.1
        

COPY  http.so  /usr/lib/mysql/plugin
RUN echo "[mariadb]\nplugin-load-add=http\n" > /etc/mysql/mariadb.conf.d/http.conf
# RUN echo "INSTALL PLUGIN http SONAME 'http.so';" > my_initdb/disks.sql

RUN echo "CREATE OR REPLACE FUNCTION http_help RETURNS STRING SONAME 'http.so';" > /docker-entrypoint-initdb.d/http.sql && \
    echo "CREATE OR REPLACE FUNCTION http_raw RETURNS STRING SONAME 'http.so';" >> /docker-entrypoint-initdb.d/http.sql && \
    echo "CREATE OR REPLACE FUNCTION http_get RETURNS STRING SONAME 'http.so';" >> /docker-entrypoint-initdb.d/http.sql && \
    echo "CREATE OR REPLACE FUNCTION http_post RETURNS STRING SONAME 'http.so';" >> /docker-entrypoint-initdb.d/http.sql
#清理  
RUN   rm -rf /var/cache/

# sudo  docker build -t mymariadb:1.0 .
